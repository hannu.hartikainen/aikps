#!/usr/bin/env python

import subprocess
import sys

def get_player_choice(player, previous_rounds, other_player):
    rounds_snippets = "".join(f',\n["{a}","{b}"]' for a,b in previous_rounds)
    json = f'[{len(previous_rounds)},"{other_player}"{rounds_snippets}]'
    proc = subprocess.run(player, input=json.encode(), capture_output=True)
    return chr(proc.stdout[0])

def swapped(pairs):
    return [(b,a) for a,b in pairs]

def main(round_count, player1, player2):
    rounds = []
    score_1 = score_2 = 0
    while max(score_1, score_2) < round_count:
        a = get_player_choice(player1, rounds, player2)
        b = get_player_choice(player2, swapped(rounds), player1)
        if a != b:
            if a+b in ("KP", "PS", "SK"):
                score_2 += 1
            else:
                score_1 += 1
        print(f"{a} - {b}         {score_1:3d} -{score_2:3d}")
        rounds.append((a,b))

if __name__ == '__main__':
    assert len(sys.argv) == 4
    main(int(sys.argv[1]), sys.argv[2], sys.argv[3])