# AI KPS Tournament draft / RFC

## Rules

- Each AI must be an executable (TBD)
- The AI receives data of the current match up until this point in *exactly* the specified format.
- The AI must respond with either 'K', 'P' or 'S' within given resource limits (TBD). Only the first printed character is considered.
- The AI *must* be fully deterministic against a given input. Ie. no randomness or reading the environment.
- The AI *must not* contain hard-coded patterns longer than 10 choices.

## Input format

The current match situation is given to an AI program as standard input (stdin). The format is intentionally valid JSON but also easy to parse manually.

The first line contains the count of played rounds and the name of the opponent.

If rounds have been played, each round is on its own row in the format `[my_decision, opponents_decision]`.

Example:
```json
[7,"examples/rocker.py",
["S","K"],
["P","K"],
["P","K"],
["S","P"],
["S","S"],
["K","K"],
["P","K"]]
```

## Usage

`./aikps.py <match_length> <ai_1> <ai_2>`

Example:
```
$ ./aikps.py 5 examples/rocker.py examples/balancer.py
K - S           1 -  0
K - P           1 -  1
K - P           1 -  2
P - S           1 -  3
S - S           1 -  3
K - K           1 -  3
K - P           1 -  4
K - P           1 -  5
```

