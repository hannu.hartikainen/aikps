#!/usr/bin/env python

import json
import sys

from collections import Counter

def main():
    round_count, _, *rounds = json.loads(sys.stdin.read())
    if round_count == 0:
        print("S")
        return
    choices = [choice for round in rounds for choice in round]
    counter = Counter(choices)
    counts = {choice: counter[choice] for choice in ('K', 'P', 'S')}
    print(min(counts, key=counts.get))

main()